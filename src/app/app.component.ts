import {Component, OnInit} from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public navigate: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  sideMenu() {
    this.navigate = [
      {
        title: 'Todas',
        url: '/home/todas',
        icon: '../assets/all.png'
      },
      {
        title: 'Yamaha',
        url: '/home/yamaha',
        icon: '../assets/yamaha-logo.png'
      },
      {
        title: 'Ducati',
        url: '/home/ducati',
        icon: '../assets/ducati-logo.png'
      },
      {
        title: 'honda',
        url: '/home/honda',
        icon: '../assets/honda-logo.png'
      }
    ];
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.navigate.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
