import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Bike} from '../home/home.page';
import {AlertController, NavController} from '@ionic/angular';
@Component({
  selector: 'app-bike-detail',
  templateUrl: './bike-detail.page.html',
  styleUrls: ['./bike-detail.page.scss'],
})
export class BikeDetailPage implements OnInit {

  bike: Bike;
  mBike: Bike;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private router: Router, private alertController: AlertController, private navCtrl: NavController) {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.bike = this.router.getCurrentNavigation().extras.state.params;
      }
    });
  }

  ngOnInit() {
    this.mBike = this.bike;
  }

  deleteBike(id: number) {
    fetch('http://motos.puigverd.org:80/moto/' + id, {
      method: 'DELETE',
      headers: {}
    })
        .then(response => {
          this.goHome();
          console.log(response);
        })
        .catch(err => {
          console.log(err);
        });
  }

  goHome() {
    this.router.navigate(['home/todas']).then(r => console.log(r));

  }

  async presentAlertMultipleButtons() {
    const [alert] = await Promise.all([this.alertController.create({
      header: '¡Atención!',
      message: '¿Estás seguro que quieres borrar la moto?',
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Delete',
          handler: () => {this.deleteBike(this.mBike.id); }

        }

      ]
    })]);

    await alert.present();
  }


}
