import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddBikePage } from './add-bike.page';

describe('AddBikePage', () => {
  let component: AddBikePage;
  let fixture: ComponentFixture<AddBikePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBikePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddBikePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
