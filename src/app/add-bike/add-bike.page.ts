import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-bike',
  templateUrl: './add-bike.page.html',
  styleUrls: ['./add-bike.page.scss'],
})
export class AddBikePage implements OnInit {
  brand: string;
  model: string;
  year: string;
  url: string;
  price: string;

  constructor(private router: Router) { }

  goHome() {
    this.router.navigate(['home/todas']).then(r => console.log(r));

  }
  addBike() {
    fetch('http://motos.puigverd.org:80/moto', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        marca : this.brand,
        modelo : this.model,
        year : this.year,
        foto : this.url,
        precio : this.price,
      })
    })
        .then(response => {
          this.goHome();
          console.log(response);
        })
        .catch(err => {
          console.log(err);
        });
  }
  ngOnInit() {
    
  }

}
