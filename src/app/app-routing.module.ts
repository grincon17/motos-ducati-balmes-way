import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home/Todas', pathMatch: 'full' },
  { path: 'home/:id',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'bike-detail',
    loadChildren: () => import('./bike-detail/bike-detail.module').then( m => m.BikeDetailPageModule)
  },
  {
    path: 'add-bike',
    loadChildren: () => import('./add-bike/add-bike.module').then( m => m.AddBikePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
