import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public home: string;
  public bikes: Bike[];
  public filter: string;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.home = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.home.toLowerCase() === 'todas') {
      this.filter = 'motos';
    } else if (this.home.toLowerCase() === 'yamaha') {
      this.filter = 'motos?marca=yamaha';
    } else if (this.home.toLowerCase() === 'ducati') {
      this.filter = 'motos?marca=ducati';
    } else {
      this.filter = 'motos?marca=honda';
    }

  }

  ngOnInit(): void {
    this.ionViewWillEnter();
  }

  ionViewWillEnter() {
    this.getJson().then(r => console.log(r));
  }

  async getJson() {
    const answer = await fetch('http://motos.puigverd.org/' + this.filter, {mode: 'no-cors'});
    this.bikes = await answer.json();
    console.log(this.bikes);
  }

  goToDetail(bike: Bike) {

    const navigationExtras: NavigationExtras = {
      state: {
        params: bike
      }
    };
    this.router.navigate(['bike-detail'], navigationExtras);
  }

  goToAddBike() {
    this.router.navigate(['add-bike']);
  }

}
export interface Bike {
  id: number;
  marca: string;
  modelo: string;
  any: number;
  foto: string;
  precio: string;
  descripcion: string;
}
